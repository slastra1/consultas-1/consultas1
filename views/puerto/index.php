<?php

use app\models\Puerto;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Puertos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puerto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Puerto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nompuerto',
            'altura',
            'categoria',
            'pendiente',
            'numetapa',
            //'dorsal',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Puerto $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nompuerto' => $model->nompuerto]);
                 }
            ],
        ],
    ]); ?>


</div>
