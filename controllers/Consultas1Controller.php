<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Ciclista;

class Consultas1Controller extends Controller
{
    public function actionIndex()
    {
        // Puedes realizar consultas a la base de datos aquí y pasar los resultados a la vista
        $ciclistas = Ciclista::find()->all();

        return $this->render('index', [
            'ciclistas' => $ciclistas,
        ]);
    }
}

